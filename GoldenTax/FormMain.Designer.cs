﻿namespace GoldenTax
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.statusBar = new System.Windows.Forms.StatusStrip();
        	this.menuMain = new System.Windows.Forms.MenuStrip();
        	this.ToolStripMenuItemExport = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemImport = new System.Windows.Forms.ToolStripMenuItem();
        	this.ToolStripMenuItemAbout = new System.Windows.Forms.ToolStripMenuItem();
        	this.menuMain.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// statusBar
        	// 
        	this.statusBar.Location = new System.Drawing.Point(0, 540);
        	this.statusBar.Name = "statusBar";
        	this.statusBar.Size = new System.Drawing.Size(784, 22);
        	this.statusBar.TabIndex = 0;
        	this.statusBar.Text = "状态栏";
        	// 
        	// menuMain
        	// 
        	this.menuMain.AllowMerge = false;
        	this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.ToolStripMenuItemExport,
			this.ToolStripMenuItemImport,
			this.ToolStripMenuItemAbout});
        	this.menuMain.Location = new System.Drawing.Point(0, 0);
        	this.menuMain.Name = "menuMain";
        	this.menuMain.Size = new System.Drawing.Size(784, 25);
        	this.menuMain.TabIndex = 2;
        	this.menuMain.Text = "menuStrip1";
        	this.menuMain.ItemAdded += new System.Windows.Forms.ToolStripItemEventHandler(this.menuMain_ItemAdded);
        	// 
        	// ToolStripMenuItemExport
        	// 
        	this.ToolStripMenuItemExport.Name = "ToolStripMenuItemExport";
        	this.ToolStripMenuItemExport.Size = new System.Drawing.Size(107, 21);
        	this.ToolStripMenuItemExport.Text = "金蝶发票导出(&E)";
        	this.ToolStripMenuItemExport.Click += new System.EventHandler(this.ToolStripMenuItemExport_Click);
        	// 
        	// ToolStripMenuItemImport
        	// 
        	this.ToolStripMenuItemImport.Name = "ToolStripMenuItemImport";
        	this.ToolStripMenuItemImport.Size = new System.Drawing.Size(104, 21);
        	this.ToolStripMenuItemImport.Text = "金税发票回写(&I)";
        	this.ToolStripMenuItemImport.Click += new System.EventHandler(this.ToolStripMenuItemImport_Click);
        	// 
        	// ToolStripMenuItemAbout
        	// 
        	this.ToolStripMenuItemAbout.Name = "ToolStripMenuItemAbout";
        	this.ToolStripMenuItemAbout.Size = new System.Drawing.Size(60, 21);
        	this.ToolStripMenuItemAbout.Text = "关于(&A)";
        	this.ToolStripMenuItemAbout.Click += new System.EventHandler(this.ToolStripMenuItemAbout_Click);
        	// 
        	// FormMain
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(784, 562);
        	this.Controls.Add(this.statusBar);
        	this.Controls.Add(this.menuMain);
        	this.IsMdiContainer = true;
        	this.MainMenuStrip = this.menuMain;
        	this.Name = "FormMain";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "开票处理接口 87627850@qq.com";
        	this.menuMain.ResumeLayout(false);
        	this.menuMain.PerformLayout();
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemExport;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemImport;
        private System.Windows.Forms.ToolStripMenuItem ToolStripMenuItemAbout;

    }
}