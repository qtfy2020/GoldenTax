﻿namespace GoldenTax
{
    partial class FormAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.RichTextBox richTextBoxAbout;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.richTextBoxAbout = new System.Windows.Forms.RichTextBox();
        	this.SuspendLayout();
        	// 
        	// richTextBoxAbout
        	// 
        	this.richTextBoxAbout.Dock = System.Windows.Forms.DockStyle.Fill;
        	this.richTextBoxAbout.ForeColor = System.Drawing.SystemColors.MenuHighlight;
        	this.richTextBoxAbout.Location = new System.Drawing.Point(0, 0);
        	this.richTextBoxAbout.Name = "richTextBoxAbout";
        	this.richTextBoxAbout.ReadOnly = true;
        	this.richTextBoxAbout.Size = new System.Drawing.Size(784, 562);
        	this.richTextBoxAbout.TabIndex = 0;
        	this.richTextBoxAbout.Text = "鹰峰电子K3发票导入航天信息开票软件工具\n\n开发：87627850@qq.com\n\nV1.0 发版日期：20150316";
        	// 
        	// FormAbout
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(784, 562);
        	this.Controls.Add(this.richTextBoxAbout);
        	this.Name = "FormAbout";
        	this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        	this.Text = "关于";
        	this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        	this.ResumeLayout(false);

        }

        #endregion
    }
}