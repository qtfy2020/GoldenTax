﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace ParseLib
{
    /// <summary>
    /// 发票信息表头
    /// </summary>
    public class InvoiceHeader
    {
        /// <summary>
        /// 作废标志
        /// </summary>
        public string FIsDisable { get; set; }

        /// <summary>
        /// 清单标志（超过8条单据明细将会有清单生成）
        /// </summary>
        public string FIsDetail { get; set; }

        /// <summary>
        /// 未知标志
        /// </summary>
        public string FTag { get; set; }

        /// <summary>
        /// 发票代码[正式]：3100144130
        /// </summary>
        public string FInvoiceCode { get; set; }

        /// <summary>
        /// 发票号码：11254568
        /// </summary>
        public string FInvoiceNumber { get; set; }

        /// <summary>
        /// 明细数据行数
        /// </summary>
        public string FDetailNumber { get; set; }

        /// <summary>
        /// 开票日期：20150309
        /// </summary>
        public string FDate { get; set; }

        /// <summary>
        /// 月份：03
        /// </summary>
        public string FMonth { get; set; }

        /// <summary>
        /// K3临时虚拟发票号:15030710
        /// </summary>
        public string FViutual { get; set; }

        /// <summary>
        /// 不含税金额（合计金额）
        /// </summary>
        public decimal FAmount { get; set; }

        /// <summary>
        /// 税率
        /// </summary>
        public decimal FRate { get; set; }

        /// <summary>
        /// 税额
        /// </summary>
        public decimal FTax { get; set; }

        /// <summary>
        /// 购方名称
        /// </summary>
        public string FCustomer { get; set; }

        /// <summary>
        /// 购方税号
        /// </summary>
        public string FCusDuty { get; set; }

        /// <summary>
        /// 购方地址电话
        /// </summary>
        public string FCusAddress { get; set; }

        /// <summary>
        /// 购方银行账号
        /// </summary>
        public string FCusBank { get; set; }

        /// <summary>
        /// 销方名称
        /// </summary>
        public string FCompany { get; set; }

        /// <summary>
        /// 销方税号
        /// </summary>
        public string FComDuty { get; set; }

        /// <summary>
        /// 销方地址电话
        /// </summary>
        public string FComAddress { get; set; }

        /// <summary>
        /// 销方银行
        /// </summary>
        public string FComBank { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string FNotes { get; set; }

        /// <summary>
        /// 开票人
        /// </summary>
        public string FCreater { get; set; }

        /// <summary>
        /// 复核人
        /// </summary>
        public string FChecker { get; set; }

        /// <summary>
        /// 收款人
        /// </summary>
        public string FReceiver { get; set; }

    }
}
